.. _installation: 

Installing SATIE
================

To install SATIE, first install `SuperCollider <https://supercollider.github.io/>`__ - you may use the `Metalab PPA <https://launchpad.net/~sat-metalab/+archive/ubuntu/metalab/>`__. 

Next, execute the following line of code in the SuperCollider IDE:

.. code-block:: supercollider

    Quarks.install("SATIE");
