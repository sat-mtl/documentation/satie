.. _basics:

============
SATIE Basics
============

*Basic SATIE tutorial with examples*

First steps
-----------

.. note::
    SATIE takes advantage of parallel processing provided by supernova server therefore it is a preferred setup and examples use it by default.

Before using SATIE, we need to create a configuration. The configuration will typically specify the speaker layout, number of effect busses to use, etc. We can also configure the supernova server options, which are held in serverOptions variable. SATIE will prepare the synthdefs on the server and later we will be able to control their life cycle.

.. code-block:: supercollider

    (
    // define a server, SATIE needs it
    s = Server.supernova.local;
    // instantiate a SatieConfiguration. Here we will use a stereo spatializer
    ~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
    // list possible listeners:
    ~satieConfiguration.spatializers.keys;
    // instantiate SATIE renderer and pass it the configuration
    ~satie = Satie.new(~satieConfiguration);
    ~satie.waitForBoot({
	    // display some information
	    s.meter;
	    s.makeGui;
	    s.plotTree;
    })
    )

The above is somewhat a minimum to get SATIE running. We will use this simple configuration for the examples that follow.

.. tip:: 
    It might be useful to peek at the list of SynthDefs that SATIE prepared on boot up. Evaluating the following line will open a window showing all SynthDefs and their parameters:

.. code-block:: supercollider
    
        ~satie.synthDescLib.browse

Now, let's make some noise. We can create instances of synths and pass them some initial arguments.

.. code-block:: supercollider

    (
    // create some test sound instances in the default group
    20.do( { arg item;
	    var nodename=("testNode"++"_"++item).asSymbol;
	    var synth;
	    synth = ~satie.makeSourceInstance(nodename.asSymbol, \dustyRez, \default, synthArgs: [\density, 15, \attack, 25]);
	    synth.set(\gainDB, rrand(-50, -5), \freq, rrand(50, 500));
    });
    )

.. note::
    All satie source instances are created with a default gain of -99dB to avoid sudden loud sounds. 
    In order to hear sound, you must explicitly set the STRONG::gainDB:: parameter to a higher value.

Now, we can control each instance individually:

.. code-block:: supercollider

    // set random frequency to each instance
    (
    20.do( {|item|
	    var nodename=("testNode"++"_"++item);
	    ~satie.groupInstances[\default][nodename.asSymbol].set(\freq, rrand(200, 2000));
    });
    )

    // set different gain level to each instance
    (
    20.do( {|item|
	    var nodename=("testNode"++"_"++item);
	    ~satie.groupInstances[\default][nodename.asSymbol].set(\gainDB, rrand(-100, -10));
    });
    )

We can also control the synth instances by setting properties to the entire group (more details about SATIE link::#Groups:: later in this document.):

.. code-block:: supercollider

    (
    ~satie.groups[\default].set(\gainDB, -99);
    ~satie.groups[\default].set(\gainDB, -18, \freq, 220);
    )


And, of course, we can change all spatialization parameters in one message:

.. code-block:: supercollider

    (
    20.do( {|item|
	    var nodename=("testNode"++"_"++item);
	    ~satie.groupInstances[\default][nodename.asSymbol].set(
		    \gainDB, rrand(-50, -10),
		    \aziDeg, rrand(-90, 90),
		    \eleDeg, rrand(-90, 90),
		    \delayMs, rrand(1,100),
		    \lpHz, rrand(10000, 18000),
		    \hpHz, rrand(60, 200),
		    \spread, rrand(0, 1)
	    );
    });
    )


.. code-block:: supercollider

    // clean test instances
    (
    20.do( { |item|
	    ~satie.cleanInstance(("testNode"++"_"++item).asSymbol);
    });
    )

Groups
------

Groups facilitate updating properties of sound objects in bulk. SATIE creates two groups automatically: soft::default:: and soft::defaultFX::. All SATIE synth and effect instances will be placed in the respective group. User can create additional groups.

.. code-block:: supercollider

    (
    // create a new group
    ~satie.makeSatieGroup(\synths);
    // add some synths to the group and give them random frequencies, loudness and position
    20.do( { arg item;
	    var nodename=("test_tone"++"_"++item).asSymbol;
	    var synth;
	    synth = ~satie.makeSourceInstance(nodename.asSymbol, \testtone, \synths, synthArgs: [
		    \sfreq, rrand(200, 800),
		    \gainDB, rrand(-60, -30),
		    \azideg, rrand(-90, 90)]);
    });
    )
    
    // set volume of all objects
    ~satie.groups[\synths].set(\gainDB, -60);

    // set frequency of all objects
    ~satie.groups[\synths].set(\sfreq, 360);

    // add another instance of a synth to the group
    ~satie.makeSourceInstance(\solo, \misDrone, \synths, [\gainDB, -20]);

    // set parameters of one instance
    ~satie.groupInstances[\synths][\solo].set(\freq, 800);

    // clean instance
    ~satie.cleanInstance(\solo, \synths);

    // delete the group and all its children
    ~satie.killSatieGroup(\synths);

Use case: Reading 5.1 audio input
---------------------------------

This example converts a 6 channel audio input that corresponds to 5.1 to stereo. The 5.1 channels should be in this order: Front-L, Center, Front-R, Surround-L, Surround-R, LFE.


.. code-block:: supercollider

    (
    // define a server, SATIE needs it
    s = Server.supernova.local;
    // instantiate a SatieConfiguration. Here we will use a stereo spatializer
    ~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
    // list possible listeners:
    ~satieConfiguration.spatializers.keys;

    // change input/output channels
    ~satieConfiguration.serverOptions.numOutputBusChannels = 2;
    ~satieConfiguration.serverOptions.numInputBusChannels = 6;

    // instantiate SATIE renderer and pass it the configuration
    ~satie = Satie.new(~satieConfiguration);
    ~satie.waitForBoot({
        // display some information
        s.meter;
        s.makeGui;
        s.plotTree;
    })
    )

    // Create 1 input for each channel
    6.do({|item|
    	~satie.makeSourceInstance(("channel_"++item).asSymbol, \MonoIn, \default, synthArgs:[\bus:item]);
    });

    // Change the azimuth for every mono channel to the corresponding position in 5.1
    ~satie.groupInstances[\default][\channel_0].set(\aziDeg,-30);
    ~satie.groupInstances[\default][\channel_1].set(\aziDeg,0);
    ~satie.groupInstances[\default][\channel_2].set(\aziDeg,30);
    ~satie.groupInstances[\default][\channel_3].set(\aziDeg,-110);
    ~satie.groupInstances[\default][\channel_4].set(\aziDeg,110);
    ~satie.groupInstances[\default][\channel_5].set(\aziDeg,0);

    // Start the playback
    ~satie.groups[\default].set(\gainDB, -4.77, \gate, 1);


Use case: Play a mono sound file
--------------------------------

This example spatializes a mono sound file.

.. code-block:: supercollider

    (
    // define a server, SATIE needs it
    s = Server.supernova.local;
    // instantiate a SatieConfiguration. Here we will use a stereo spatializer
    ~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
    
    // change input/output channels
    ~satieConfiguration.serverOptions.numOutputBusChannels = 2;
    ~satieConfiguration.serverOptions.numInputBusChannels = 2;

    // instantiate SATIE renderer and pass it the configuration
    ~satie = Satie.new(~satieConfiguration);
    ~satie.waitForBoot({
        s.meter;
        s.makeGui;
        s.plotTree;
    })
    )
    
    // cue file and play it
    (
    ~monoFile = Buffer.cueSoundFile(~satie.config.server, Platform.resourceDir + "/" + "sounds/a11wlk01.wav", 0, 1);

    // instantiate
    ~satie.makeSourceInstance(\monof, \sndFile, \default, synthArgs: [\bufnum: ~monoFile, \loop, 1]);
    ~satie.groupInstances[\default][\monof].set(\gainDB, -10, \aziDeg, -120, \eleDeg, 15, \t_trig, 1, \loop, 1);
    )

    // cue again or another
    ~monoFile.cueSoundFile(Platform.resourceDir +"/"+ "sounds/a11wlk01.wav");

    // clean up
    ~satie.clearScene

We are now ready to explore SATIE and its features.
