.. _cheatsheet:

=================
SATIE Cheat Sheet
=================

This is a cheat sheet for some most common use cases of SATIE. It contains most commonly used OSC messages with some explanations.
Please refer to :doc:`OSC API <../reference/api_osc>` for a complete OSC documentation.

An important notion to keep in mind is that of a *source*. It refers to a sound generator of any type, i.e. a process that is capable of producing some kind audio signal on its own. This is in complement to an *effect* which does not produce a signal on its own but modfies a *source*'s signal.

----------------------
Creating audio sources
----------------------

Audio sources are at the base of SATIE. There will be no sound without audio sources. Even live input needs to be created with an appropriate plugin.

.. option:: /satie/scene/createSource nodeName synthdefName \<groupName\> \<key1 value1 ... keyN valueN\>

Put an audio generator (source) plugin in the audio scene.

Mandatory arguments:

- nodeName : the name of the instance.
- synthDefName : the name of the synthdef registered with the system. It's typically a plugin name in the `plugins/sources` directory of the SATIE tree. Typically the name of the plugin file corresponds to the name of the synth definition, without the `.scd` extension. So `RezonPing.scd` in the folder will be `RezonPing`

Optional arguments:

- groupName : (optional) the name of the group. Default: '\\default'
- key, value : (optional) synth arguments as key value pairs

Note, that if you want to give the source some initial values, you must also use a `groupName`. `\\default` is a valid group name.

Example:
~~~~~~~~

.. option:: /satie/scene/createSource myCuteSynth RezonPing

will put `RezonPing` instrument in the audio scene. However, we will not hear it yet as all instruments are muted.

.. option:: /satie/scene/createSource myCuteSynth RezonPing default gainDB -20

will do the same but will raise the volume to -20dB at the same time.

----------------------
Audio samples handling
----------------------

Audio samples need some special treatment as they also exist as ressources on the hard-drive. Ideally, use absolute paths.

.. option:: /satie/loadSample name path

Load an audio sample into SATIE's audioSamples dictionary. Once loaded, an audio sample's name can be used as a substitute
to providing the internal buffer number for that sample when creating source/effect/process nodes.

- name: the chosen name for the audio sample
- path: the path where the audio sample is located

Example loadSample OSC message:

.. option:: /satie/loadSample mySample /home/bob/audio/mySample.wav

(but a shorthand `/satie/loadSample mySample ~/audio/mySample.wav` will also work).

Once the audio file is loaded into the memory, it does not yet appear in SATIE's scene. We need to tell a sample player to use that files audio data. The sample player plugin in SATIE is called `sndBuffer`:

Example createSource OSC message using sample 'mySample':

.. option:: /satie/scene/createSource mySource sndBuffer default bufnum mySample gate 1 gainDB -20

In the above message, the playback is triggered with `gate 1` and its signal level raised with `gainDB -20`. You can pause the playbakc with `gate 0` message (see below for control messages of existing objects)

---------------------------
Controlling existing sounds
---------------------------

.. option:: /satie/source/set nodeName key1 val1 ... keyN valN

will allow to set one or more parameters on an existing source.

For example, the previously created `myCuteSynth` source can be manipulated with `set` message:

.. option:: /satie/source/set myCuteSynth aziDeg -90

will pan `myCuteSynth` hard left.

.. option:: /satie/source/set myCuteSynth aziDeg 90 freq 200

will pan it hard right and change its frequency to 200Hz.


With `/satie/\<nodeType\>/update nodeName azimuth elevation gainDB delayMS lpHz` we can control all spatialization parameters at once:

.. option:: /satie/source/update myCuteSynth -45 10 -40 20 200

will place the sound slightly left, a little over the eyes level (if set on a 3D audio setup) lower its volume, add 20ms of delay and apply a low-pass filter. The delay parameter makes sense only when sending `update` messges at high rate and allows for simulating the *doppler shift*.

------------------------------
Controlling object's life span
------------------------------

.. option:: /satie/scene/deleteNode nodeName

Delete a source, effect, process, or group

-  nodeName : the name of the node to be deleted


.. option:: /satie/scene/clear

Reset the SATIE scene.
