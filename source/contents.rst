.. SATIE documentation documentation master file, created by
   sphinx-quickstart on Thu Feb  4 11:40:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
=====================
SATIE - documentation
=====================

*SATIE - Spatial Audio Toolkit for Immersive Environments*

.. toctree::
   :maxdepth: 1
   :caption: On this site:
   
   installation/contents
   tuto/quickstart
   tuto/basics
   tuto/cheatsheet
   tuto/contents
   reference/contents
      
   Back to main page <index>
..   tuto/contents

..   guide/contents
..   description/glossary
   

..   news
..   community
   
..   faq/contents
   
..   contact

First steps with SATIE
======================

Get started with SATIE with the :doc:`SATIE Basics <tuto/basics>` tutorial before taking a look at the :doc:`SATIE Cheatsheet <tuto/cheatsheet>` for an overview of some common usages.

Documentation
=============

The documentation contains the following sections:

* :doc:`tutorials <tuto/contents>` : step-by-step guides to get started
* :doc:`reference <reference/contents>` : the complete API documentation


Search this website
===================

* :ref:`search`


