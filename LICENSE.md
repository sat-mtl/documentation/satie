# LICENSE

This documentation is licensed under a [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

All the source code (including examples and excerpt from the software) is licensed under the [GNU Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl-3.0.en.html).
